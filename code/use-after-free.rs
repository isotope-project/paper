fn use_after_free(my_value: i32) -> i32 {
    let ptr = Box::new(my_value);
    std::mem::drop(ptr);
    *ptr
}