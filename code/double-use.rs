fn double_use(v: Vec<u32>) -> (Vec<u32>, Vec<u32>) {
    let u = double_vec(v);
    (u, v)
}