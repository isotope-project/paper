/// This function allocates and then explicitly frees memory
fn explicit_drop() {
    let memory: Vec<u8> = Vec::with_capacity(8192);
    println!("{:?}", memory);
    std::mem::drop(memory);
}

/// This function does *not* leak memory, even though memory
/// is not explicitly freed, due to RAII
fn no_leak() {
    let memory: Vec<u8> = Vec::with_capacity(8192);
    println!("{:?}", memory);
    // An implicit call to `std::mem::drop(memory)` is inserted here,
    // since _memory is never destroyed in the function body, even
    // though it is used.
}

/// This function intentionally leaks memory in safe Rust
fn intentional_leak() {
    let memory: Vec<u8> = Vec::with_capacity(8192);
    println!("{:?}", memory);
    // The destructor of `memory` is never called, 
    // since it is consumed by `forget`
    std::mem::forget(memory);
}