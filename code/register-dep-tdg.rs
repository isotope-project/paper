enum DepKind {
    /// A dependency which consumes its input
    Consume,
    /// A dependency which uses its input *without* consuming it
    Use,
    /// A dependency which borrows its input
    Borrow,
}

impl Tdg {
    /// Get the beginning of a resource
    fn begin(&self, resource: Resource) -> Instant;
    /// Get the consumption of a resource
    fn consumed(&self, resource: Resource) -> Instant;
    /// Get the end of a resource
    fn end(&self, resource: Resource) -> Instant;
    /// Register a temporal edge from `s` to `t` with label `strict`
    fn register_edge(&mut self, s: Instant, t: Instant, strict: bool);
}

/// Register that `t` depends on `s`
fn register_dep(tdg: &mut Tdg, s: Resource, t: Resource, kind: DepKind) {
    match kind {
        DependencyKind::Consume => {
            tdg.register_edge(tdg.consumed(s), tdg.begin(t), true);
            tdg.register_edge(tdg.end(s), tdg.consumed(t), true);
        }
        DependencyKind::Use => {
            tdg.register_edge(tdg.begin(s), tdg.begin(t), true);
            tdg.register_edge(tdg.consumed(s), tdg.begin(t), true);
        }
        DependencyKind::Borrow => {
            tdg.register_edge(tdg.begin(s), tdg.begin(t), true);
            tdg.register_edge(tdg.end(s), tdg.begin(t), true);
        }
    }
}
