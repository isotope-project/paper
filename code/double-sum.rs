/// This function doubles the value of it's argument
fn double_vec(v: Vec<u8>) -> Vec<u8>;

/// This function returns some slice of the underlying vector
fn slice_vec(v: &Vec<u8>) -> &[u8];

/// This function takes the sum of it's argument
fn hash(s: &[u8]) -> u64;

fn double_sum(v: Vec<u8>) -> (Vec<u8>, u64) {
    let s: &[u8] = slice_vec(&v);
    let h: u64 = sum(s);
    let u = double_vec(v);
    (u, h)
}