#include<stddef.h>
#include<stdint.h>

struct VecU8 {
    size_t capacity;
    size_t len;
    uint8_t* data;
};

struct SliceU8 {
    size_t len;
    const uint8_t* data;
};

struct VecU8_and_U64 {
    struct VecU8 u;
    uint64_t h;
};

struct VecU8 double_vec(struct VecU8 v);

struct SliceU8 slice_vec(const struct VecU8* v);

uint64_t sum(struct SliceU8 s);

struct VecU8_and_U64 double_sum(struct VecU8 v) {
    struct SliceU8 s = slice_vec(&v);
    uint64_t h = sum(s);
    struct VecU8 u = double_vec(v);
    struct VecU8_and_U64 r = { .u =  u, .h =  h };
    return r;
}

struct VecU8_and_U64 double_sum_bad(struct VecU8 v) {
    struct SliceU8 s = slice_vec(&v);
    struct VecU8 u = double_vec(v);
    uint64_t h = sum(s);
    struct VecU8_and_U64 r = { .u =  u, .h =  h };
    return r;
}