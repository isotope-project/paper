\documentclass[sigplan, screen]{acmart}
\citestyle{acmauthoryear}
\acmConference[WITS'22]{Workshop on the Implementation of Type Systems}{January 22}{Philadelphia, PA, USA}

\title{Typechecking up to Congruence}
\author{Jad Elkhaleq Ghalayini}
\affiliation{%
    \institution{University of Cambridge}
    \department{Department of Computer Science and Technology}
    \streetaddress{William Gates Building}
    \city{Cambridge}
    \country{United Kingdom}
}
\email{jeg74@cl.cam.ac.uk}

\begin{document}

\maketitle

The majority of existing dependent type theories compare terms modulo some reduction relation, which, by reflecting computation directly in the type system, allows for techniques such as proof by reflection. Consequently, term reduction is one of the most time-consuming and complex parts of dependent type checking and inference, making highly-optimized evaluators and sophisticated evaluation strategies essential components of most modern dependently typed language implementations. While we could theoretically compare terms by simply comparing their normal forms when working in a strongly normalizing language, this strategy raises serious efficiency concerns. Hence, effective type-checking often requires limiting how much normalization the type-checker will perform when comparing terms, such as imposing arbitrary limits on the number of reductions or not reducing fixpoints and terms in head-normal form.

In \cite{putc}, Sjöberg and Weirich handle potentially nonterminating terms by taking the somewhat radical step of designing a dependently typed language not equipped with automatic \(\beta\)-reduction at all, by instead performing type-checking modulo the congruence closure of some context of explicitly specified equalities, which may be independently verified. Hence, the user may specify a custom evaluation strategy for hard-to-compare terms and take advantage of equality's congruence properties to, e.g., efficiently substitute under binders. To recover the "usual" experience of dependently typed programming, the authors then describe a surface language that elaborates into the explicitly annotated core language with appropriate equalities automatically inferred, preserving the ability to provide custom equalities while reclaiming the convenience of automatic beta-reduction. 
 
In this work, we propose an extension of this method applicable to various general-purpose type systems. In particular, given a variant of the (dependently-typed) lambda calculus satisfying some basic properties, we may construct a modified calculus in which we compare and type-check terms within an "equivalence context" of explicit equalities (which may be checked by a variety of methods, including via optimized normalization), sans any notion of automatic reduction or normalization. We then provide an algorithm that, given a term in the original calculus, produces a term in the modified calculus and a set of equality constraints that are satisfied if and only if the original term type-checks. We may also similarly lower the problems of type inference, term comparison, and term unification to the modified language. Therefore, we may implement the original calculus "under the hood" by lowering the modified calculus, with little to no changes to the original type theory or semantics, preserving compatibility with programs in the original language while giving users the option to add manual equality annotations. We believe that this method can significantly improve the speed with which sizeable proof developments can be type-checked. In particular, a critical feature of our design is the ability to potentially verify equalities in parallel, taking advantage of modern multiprocessor design. We have used this method to develop an experimental congruence-based implementation of the Calculus of Inductive Constructions written in Rust, "isotope," which we hope to demonstrate.

While a significant advantage of our method is that we may directly apply it to existing type theories, we may also use it to dynamically introduce new judgemental equalities by tweaking the reduction and congruence relations. As an example, we plan to demonstrate how the techniques of this paper are compatible with other reduction relations, including the general ones described in \cite{taming-the-rew} as well as relations between terms such as associativity, commutativity, and idempotence, as a relatively straightforward extension of "isotope."

\bibliographystyle{ACM-Reference-Format}
\bibliography{references}

\end{document}